var express = require('express');
var morgan = require('morgan');
const cors = require('cors');
const helmet = require('helmet');
const compression = require('compression');
var app = express();
app.use(cors());
app.use(helmet());
app.use(compression());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(morgan('dev'));

app.use(express.static(__dirname + '/public'));

app.use('/api', require('./api'));

module.exports = app;
