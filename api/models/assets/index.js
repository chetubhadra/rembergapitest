const { mongoose } = require('../../../configs');
const { Schema } = mongoose;

const Assets = {
  name: String,
  company_name: String,
  owner: String,
  image: String,
};

module.exports = mongoose.model('Assets', new Schema(Assets, { timestamps: true }));
