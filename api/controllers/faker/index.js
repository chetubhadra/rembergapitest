const { Assets } = require('../../models');

var faker = require('faker');
module.exports = {
  storeFakeData: (req, res) => {
    const arr = [];
    for (let index = 0; index < 1000; index++) {
      let Obj = {
        owner: faker.name.firstName() + ' ' + faker.name.lastName(),
        company_name: faker.company.companyName(),
        name: faker.commerce.productName(),
        image: faker.image.abstract(),
      };
      arr.push(Obj);
      if (index === 999) callStore();
    }
    function callStore() {
      console.log('arr.length', arr.length);
      return Assets.insertMany(arr).then(console.log('Fake Data inserted.'));
    }
  },
};
